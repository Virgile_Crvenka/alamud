from .event import Event2


class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        if not self.object.has_prop("eatable"):
            self.add_prop("object-not-eatable")
            return self.eat_failed()
        if self.object not in self.actor:
            return self.eat_failed()
        self.actor.remove(self.object)
        self.inform("eat")

    def take_failed(self):
        self.fail()
        self.inform("eat.failed")
